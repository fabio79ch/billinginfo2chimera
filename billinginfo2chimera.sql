-- \c chimera

-- 9.3 News
-- http://www.postgresql.org/docs/9.3/static/postgres-fdw.html
CREATE EXTENSION postgres_fdw;

DROP   SERVER mylocalhost CASCADE;
CREATE SERVER mylocalhost 
       FOREIGN DATA WRAPPER postgres_fdw OPTIONS (host 'localhost', dbname 'billing', port '5432');


CREATE FOREIGN TABLE billinginfo ( 
           datestamp timestamp with time zone, 
           pnfsid    varchar(36), 
           p2p       boolean
         ) 
       SERVER mylocalhost OPTIONS (table_name 'billinginfo');

CREATE USER MAPPING FOR postgres SERVER mylocalhost;

UPDATE t_inodes 
       SET iatime = iatimebillinginfo.newer_iatime 
       FROM ( 
              SELECT   max(billinginfo.datestamp) as newer_iatime, 
                       pnfsid 
              FROM     billinginfo 
              WHERE    p2p = 'f'
              group by pnfsid 
            ) AS iatimebillinginfo 
            
       WHERE t_inodes.ipnfsid = iatimebillinginfo.pnfsid 
       AND   iatime           < iatimebillinginfo.newer_iatime
; 

-- Optional if you use https://bitbucket.org/fabio79ch/v_pnfs
--
-- refresh materialized view v_pnfs ;  
-- refresh materialized view v_pnfs_top_dirs ; 

